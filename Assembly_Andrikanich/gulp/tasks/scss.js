import dartSass from "sass";// препроцессор sass
import gulpSass from "gulp-sass"; // плагин для запуска препроцессора sass
import rename from "gulp-rename"; // плагин для переименования в".min.css"

import cleanCss from "gulp-clean-css"; // сжатие css файла
import webpcss from "gulp-webpcss"; // вывод WEBP изображения
import autoprefixer from "gulp-autoprefixer"; //к определенным свойствам будет добавление вендорных префиксов
// что позволит сделать нашу верстку более кроссбраузерной
import groupCssMediaQueries from "gulp-group-css-media-queries"; //авто групировка медиа запросов

const sass = gulpSass(dartSass);// в константу sass делаем вызов из плагина 
//"gulp-sass" с передачей непосредственного компилятора

export const scss = () => {
	return app.gulp.src(app.path.src.scss, { sourcemaps: app.isDev })
		// app.path.src.scss получаем доступ к файлу,
		// { sourcemaps: true } это карта исходника, мы будем собирать стили с нескольких scss, при возникновении ошибки, или просто в анализе какойто ошибки можно будет видеть
		// в каком именно файле этот стиль написан
		// обработка ошибок
		.pipe(app.plugins.plumber(
			app.plugins.notify.onError({
				title: "SCSS",
				message: "Error: <%= error.message %>"
			})))
		
		.pipe(app.plugins.replace(/@img\//g, `../img/`)) // обработка alias (превдонимов)
		
		.pipe(sass({
			outputStyle:"expanded"
		}))
		.pipe(
			app.plugins.if(
				app.isBuild,
				groupCssMediaQueries()
			)
	)
		.pipe(
						app.plugins.if(
				app.isBuild,
				autoprefixer({
					grid: true,
					overrideBrowserslist: ["last 3 versions"],
					cascade:true
				})
			)
		)
			.pipe(
				app.plugins.if(
					app.isBuild,
					webpcss({
						webClass: ".web",
						noWebClass: ".no-webp"
					}
					)
			)
		)
		






		// если нужно посмотреть на не сжатый дубль файла стилей раскоммент нижн строчку
		.pipe(app.gulp.dest(app.path.build.css)) // выгрузится в папку с рез

		.pipe(cleanCss()) // вызываем зжатие файла стилей
		.pipe(rename({
			extname: ".min.css"
		}))
		.pipe(app.gulp.dest(app.path.build.css))// выгружаем в папаку с результатом
		.pipe(app.plugins.browsersync.stream());// обновляем браузер
}