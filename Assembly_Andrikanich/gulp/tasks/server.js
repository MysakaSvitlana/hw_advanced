
export const server = (done) => {
	app.plugins.browsersync.init({// обращаемся к глоб переменной, находим browsersync и запускаем его
		server: {
			baseDir: `${app.path.build.html}`// базовая папка с результатом проэкта откуда нужно запустить файл
		},
		notify: false, // убираем сообщения в браузере
		port: 3000,
	});
}