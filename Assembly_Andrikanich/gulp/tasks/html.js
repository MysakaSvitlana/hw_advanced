import fileInclude from "gulp-file-include";
import webpHtmlNosvg from "gulp-webp-html-nosvg";
import versionNumber from "gulp-version-number"; // плагин добавляет ключ к файлам,
// который не позволяет их кешировать в браузере

//эта задача копирует файл из src в dist
export const html = () => {
	return app.gulp.src(app.path.src.html)// получили доступ к файлам
		.pipe(fileInclude())
		.pipe(app.plugins.replace(/@img\//g, 'img/'))
			.pipe(
				app.plugins.if(
					app.isBuild,
					versionNumber({
						value: `%DT%`,
						append: {
							key: `_v`,
							cover: 0,
							to: ["css", "js"],
						},
						output: {
							file: "gulp/version.json",
						},
					})
				)
			)
//подключаем настройки, которые устан текущ дату и подключ
		// ее к файлам.js и.html и за счёт этого обновляется кэш,
		// видно изменения после правок
		.pipe(app.gulp.dest(app.path.build.html))// отправляем файлы на результат
		.pipe(app.plugins.browsersync.stream());	
}