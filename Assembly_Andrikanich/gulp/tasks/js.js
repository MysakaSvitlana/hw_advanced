import webpack from "webpack-stream"; 
// наблюдатель который покажет в каком файле проблема и выведет ее
export const js = () => {
	return app.gulp.src(app.path.src.js, { sourcemaps: app.isDev })
		.pipe(app.plugins.plumber(
			app.plugins.notify.onError({
				title: "JS",
				message: "Error: <%= error.message %>"
			}))
	)
	// что бы в синтаксисе ES6 мы могли подключать все файлы  проэкту 
	//наблюдать за ними и импортировать одного gulp не достаточно нужен webpack
		.pipe(webpack({
			mode: app.isBuid ? "production" : "development", // режим разработчика
			output: {
				filename: "app.min.js",
			}
		}))
		.pipe(app.gulp.dest(app.path.build.js))// выгруж файлы в папку с результатом
		.pipe(app.plugins.browsersync.stream());// обновляем файлы
}