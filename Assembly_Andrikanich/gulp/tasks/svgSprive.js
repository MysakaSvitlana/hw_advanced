import svgSprite from "gulp-svg-sprite";
// плагин, который регулирует загрузку svg
export const svgSprive = () => {
	return app.gulp.src(`${app.path.src.svgicons}`, {})
		.pipe(app.plugins.plumber(
			app.plugins.notify.onError({
				title: "SVG",
				message: "Error: <%= error.message %>"
			}))
		)
		.pipe(svgSprite({
			mode: {
				stack: {
					sprite: `../icons/icons.svg`,
					// Создать страницу с перечнем иконок
					example: true// будет создан файл html с preview всех svg файлов
				}
			},
		
		}))
		.pipe(app.gulp.dest(`${app.path.build.images}`));
}