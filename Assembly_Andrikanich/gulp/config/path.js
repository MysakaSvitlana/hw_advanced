const buildFolder = "./dist";
// Получаем имя папки проэкта
import * as nodePath from "path";
const rootFolder = nodePath.basename(nodePath.resolve());
const srcFolder = "./src";

export const path = {
	build: {
		js: `${buildFolder}/js/`,
		css: `${buildFolder}/css/`,
		html: `${buildFolder}/`, // помещать .html файлы в корень папки с результатом
		files: `${buildFolder}/files/`,
		images: `${buildFolder}/img/`,
		fonts: `${buildFolder}/fonts/`,
	
	},
	src: {
		js: `${srcFolder}/js/app.js`,
		images: `${srcFolder}/img/**/*.{jpg, jpeg, png, gif, webp}`,
		svg: `${srcFolder}/img/**/*.svg`,
		scss:`${srcFolder}/scss/style.scss`,
		html: `${srcFolder}/*.html`, //папка с исходниками, абсолютно любое название файла с форматом которые находятся в папке src
		fonts: `${srcFolder}/fonts/`,
		files: `${srcFolder}/files/**/*.*`,
		svgicons: `${srcFolder}/svgicons/*.svg`,
	},
	watch: {
		js: `${srcFolder}/js/**/*.js`,
		scss: `${srcFolder}/scss/**/*.scss/`,//наблюдаем за .html файлами, которые находятся и в папке src и в других папках для того что-бы собирать части .html (например: header.html, nav.html)
		html: `${srcFolder}/**/*.html`, //путь с маской за какимим файлами нам необходимо следить, в даном случае путь и маски будут точно такие же как и в обьекте ікс
		files:`${srcFolder}/files/**/*.*`,
		images: `${srcFolder}/img/**/*.{jpg, jpeg, png, svg, gif, ico, webp}`,
		svg: `${srcFolder}/img/**/*.svg`,
	},
	clean: buildFolder,
	buildFolder: buildFolder,
	srcFolder: srcFolder,
	rootFolder: rootFolder,
	ftp:""
}