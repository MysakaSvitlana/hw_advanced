// В собраные файлы попадает псевдоним с package.json, где я прописывала замену через 
//alias "@img": "${folder}/src/img", "@scss": "${folder}/src/scss", "@js": "${folder}/src/js"
// нужна донастройка задачи html с помощью плагина , которую нужно исп и для других файлов
import replace from "gulp-replace"; // Поиск и замена
import plumber from "gulp-plumber";// обработка ошибок
import notify from "gulp-notify";// сообщения (подсказки)
import browsersync from "browser-sync";//локальный сервер
import newer from "gulp-newer";// проверка обновлений, обновилась ли картинка действительно, будут создаваться только те картинки которых еще нет в папке с результатом
import ifPlugin from "gulp-if";// Условное ветвление
// Создай обьект в который будешь
//экспортировать, в нем собираем наши плагины
export const plugins = {
	replace: replace,
	plumber: plumber,
	notify: notify,
	browsersync: browsersync,
	newer: newer, // проверяет обновилась ли картинка действительно, 
	//для того что бы мы не пересоздавали картинку заново, с помощью newer мі будем проверять создаваль 
	//ли наша кртинка уже, если нет то будем создавать ее в .webp
	if: ifPlugin
}
// plumber помогает обработать возникающие ошибки, возникающие при работе с тем или иным файлом
