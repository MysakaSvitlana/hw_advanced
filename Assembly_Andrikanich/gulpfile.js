// Импортируем основной модуль в обьект
import gulp from "gulp";

// Импорт путей в обьект
import { path } from "./gulp/config/path.js";
// Импорт общих плагинов в обьект
import { plugins } from "./gulp/config/plugins.js";

// Создаем глобальный обьект в котором будут пути, общие сущности
// Передаем значения в глобальную переменную, верхние обьекты в этот глобальный
// И теперь его можно исп и в будущих задачах
// переменная process.argv может хранить в себе переданный флаг, если преременная хранит флаг build - это режим продакшина, если нет - разработчика 
global.app = {
	isBuild: process.argv.includes("--build"),
  isDev: !process.argv.includes("--build"),
	path: path,
	gulp: gulp,
	plugins: plugins,
};
//Импорт задач
import { copy } from "./gulp/tasks/copy.js";
import { reset } from "./gulp/tasks/reset.js";
import { html } from "./gulp/tasks/html.js";
import { server } from "./gulp/tasks/server.js";
import { scss } from "./gulp/tasks/scss.js";
import { js } from "./gulp/tasks/js.js";
import { images } from "./gulp/tasks/images.js";
import { otfToTtf, ttfToWoff, fontsStyle } from "./gulp/tasks/fonts.js";
import { svgSprive } from "./gulp/tasks/svgSprive.js";



// Наблюдатель за изменениями в файлах
function watcher() {
	gulp.watch(path.watch.files, copy);
	gulp.watch(path.watch.html, html);
	gulp.watch(path.watch.scss, scss);
	gulp.watch(path.watch.js, js);// добавляем наблюдатели за всеми файлами и .js тоже
	gulp.watch(path.watch.images, images);
	
};
export { svgSprive };
const fonts = gulp.series(otfToTtf, ttfToWoff, fontsStyle);

//ОСНОВНЫЕ ЗАДАЧИ
// тут последовательность не нужна, поэтому паралельное выполнение
//копирование файлов из папаки files файлов, потом формирование html

const mainTasks = gulp.series(fonts, gulp.parallel(copy, html, scss, js, images));
// Постоение сценария выполнения задач, записываем его последовательность в переменную, потому что он стал сложнее и будет усложняться
// Сначала удаляется папка с результатом, потом стандарт действия по
// копированию html(mainTasks), паралельно запускается наблюдатель и сервер
const dev = gulp.series(reset, mainTasks, gulp.parallel(watcher, server));
const build = gulp.series(reset, mainTasks);
// экспорт сценариев
export { dev };
export { build };
// Выполнение сценария по умолчанию
gulp.task("default", dev);

